import java.util.*;

public class Gravitacija{
    public static void main(String[] args){
        System.out.print("OIS je zakon!");
           Scanner sc = new Scanner(System.in);

        double nadVisina = sc.nextDouble();
        double gravKonst = 6.674 * Math.pow(10, -11);
        double masaZem = 5.972 * Math.pow(10, 24);
        double polZem = 6.371 * Math.pow(10, 6);

        double gravPosp = (gravKonst*masaZem) / (Math.pow((polZem+nadVisina),2));
       //System.out. println (nadVisina);
       //System.out.println (gravPosp);
    
    System.out.printf("Nadmorska višina: %.3f m%n", nadVisina);
    System.out.printf("Gravitacijski pospešek: %.3f m/s^2%n", gravPosp);
    }
}
